=========================================================================
Python+pytest+lxml: Test suite validating messages against W3C XML Schema
=========================================================================

This repository provides an example of how to validate a DATEX II message serialized to an XML file.
Works on Linux as well as on Windows platforms.

Prerequisites
=============

- Python 3.10 or 3.11
- `Python package and dependency manager (pdm) <https://pdm.fming.dev/latest/>`_
- `git`


Installation
============

Clone the repository of the project::

    git clone git@gitlab.com:datex2/presentation/2023-virtduf-validation/pytest-validate.git

Change the directory to the root of the project::

    cd pytest-validate

Install python packages dependencies::

    pdm install

Configuration
=============
There is nothing to configure.


Usage
=====
Tests are run by calling test runner CLI `pytest`.

It has to be run within activated virtual python environment. This can be achieved:

- either by prefixing the command by `pdm run` (used in the examples below)
- or by activating the python virtual environment in any other way (not explained here)

To run the validation, do the following::

    pdm run pytest -sv tests
    ============================= test session starts ==============================
    platform linux -- Python 3.11.4, pytest-7.4.2, pluggy-1.3.0 -- /home/javl/devel/datex2/gitlab/presentation/2023-virtduf-validation/pytest-validate/.venv/bin/python
    cachedir: .pytest_cache
    rootdir: /home/javl/devel/datex2/gitlab/presentation/2023-virtduf-validation/pytest-validate
    collecting ... collected 4 items

    tests/test_validate.py::test_validate[samples/accident.xml] PASSED
    tests/test_validate.py::test_validate[samples/brokendownvehicles.xml] PASSED
    tests/test_validate.py::test_validate[samples/restriction-bkom.xml] PASSED
    tests/test_validate.py::test_validate[samples/invalid_missig_lang.xml] FAILED

    =================================== FAILURES ===================================
    ________________ test_validate[samples/invalid_missig_lang.xml] ________________

    schema = <lxml.etree.XMLSchema object at 0x7ffb1e93f9c0>
    message = <lxml.etree._ElementTree object at 0x7ffb1e995740>

        def test_validate(schema, message):
    >       assert schema.validate(message), f"Invalid: {schema.error_log}"
    E       AssertionError: Invalid: samples/invalid_missig_lang.xml:11:0:ERROR:SCHEMASV:SCHEMAV_CVC_COMPLEX_TYPE_4: Element '{http://datex2.eu/schema/2/2_0}payloadPublication': The attribute 'lang' is required but missing.
    E       assert False
    E        +  where False = <bound method _Validator.validate of <lxml.etree.XMLSchema object at 0x7ffb1e93f9c0>>(<lxml.etree._ElementTree object at 0x7ffb1e995740>)
    E        +    where <bound method _Validator.validate of <lxml.etree.XMLSchema object at 0x7ffb1e93f9c0>> = <lxml.etree.XMLSchema object at 0x7ffb1e93f9c0>.validate

    tests/test_validate.py:33: AssertionError
    =========================== short test summary info ============================
    FAILED tests/test_validate.py::test_validate[samples/invalid_missig_lang.xml]
    ========================= 1 failed, 3 passed in 0.08s ==========================

ChangeLog
=========

version 1.0.0 

- Initial version
