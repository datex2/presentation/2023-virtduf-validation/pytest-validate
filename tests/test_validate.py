from pathlib import Path

from lxml import etree

import pytest


@pytest.fixture(
    params=[
        "samples/accident.xml",
        "samples/brokendownvehicles.xml",
        "samples/restriction-bkom.xml",
        "samples/invalid_missig_lang.xml",
    ]
)
def message(request):
    """Parsed XML message (as lxml tree instance)"""
    path = Path(request.param)
    assert path.exists()
    xml_doc = etree.parse(str(path))
    return xml_doc


@pytest.fixture
def schema():
    """W3C XML Schema (lxml XMLSchema instance)"""
    path = Path("schema/schema.xsd")
    assert path.exists()
    xsd_doc = etree.parse(path)
    xml_schema = etree.XMLSchema(xsd_doc)
    return xml_schema


def test_validate(schema, message):
    """Test that the `message` conforms to `schema`"""
    assert schema.validate(message), f"Invalid: {schema.error_log}"
